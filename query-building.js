const mongoose = require('mongoose')
const Building = require('./models/Building')
const Room = require('./models/Room')
mongoose.connect('mongodb://localhost:27017/example')

async function main() {
    // Update
    // const room = await Room.findById('621c976113bf6d93be9a97a8')
    // room.capacity = 30
    // room.save()
    // console.log(room)
    // Find
    // const room = await Room.findOne({capacity: { $gte:100 } })
    // console.log(room)
    // console.log('--------------------')
    // const rooms = await Room.find({capacity: { $gte:100 } })
    // console.log(rooms)
    const newInformaticsBuilding = await Building.findById('621c976113bf6d93be9a97a6')
    const room = await Room .findById('621c976113bf6d93be9a97ac')
    const informaticsBuilding = await Building.findById(room.building)
    console.log(newInformaticsBuilding)
    console.log(room)
    console.log(informaticsBuilding)
    room.building = newInformaticsBuilding
    newInformaticsBuilding.rooms.push(room)
    informaticsBuilding.rooms.pull(room)
    room.save()
    newInformaticsBuilding.save()
    informaticsBuilding.save()
}
main().then(() => {
    console.log('Finish')
})